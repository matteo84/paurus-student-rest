insert into student (name, hash) values ('John Doe', 'b5cad945ecc4f5456b1755d0997ade17');
insert into student (name, hash) values ('Jane Doe', '52d8877b20d2bd72197080e816e19d1f');
insert into student (name, hash) values ('Joe Doe', 'e8ada6cd9bb8d2d1f3150dc383308d4d');

insert into student_class (name) values ('marketing');
insert into student_class (name) values ('machine learning');
insert into student_class (name) values ('algorithms');