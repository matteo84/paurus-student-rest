package si.matej.paurustest.task1.controller;

import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import si.matej.paurustest.task1.model.StudentClass;
import si.matej.paurustest.task1.service.StudentClassService;
import si.matej.paurustest.task1.service.StudentClassService.EnrollOperation;

@RestController
@RequestMapping("/api/class")
public class StudentClassController {

    private StudentClassService studentClassService;

    @Autowired
    public StudentClassController(final StudentClassService studentClassService) {
        this.studentClassService = studentClassService;
    }

    /***
     *
     * @return List of all StudentClass objects available
     */
    @GetMapping("/all")
    public List<StudentClass> getAll() {
        return studentClassService.getAllClasses();
    }

    /***
     * This method returns a List of all StudentClass objects that match the name passed as parameter to the method.
     * The name has to be exact for the method to find the StudentClass. Also there can be more than 1 classes
     * with the same name since no constraints were added to the StudentClass's entity name field.
     * In a real life IS we would have different search methods or flags for this method which would idicate the
     * type of search (contains, wildcard, percent matching...)
     *
     * @param name - the exact name of the StudentClass we are searching
     * @return list of all matching classes
     */
    @GetMapping("/search/findByName")
    public List<StudentClass> findByName(@RequestParam(value = "name") final String name) {
        return studentClassService.findByName(name);
    }

    /***
     * This method is used to enroll the authenticated user to the selected classes passed in the enrollClasses param
     *
     * @param enrollClasses - set of selected classes on which the user should be enrolled
     */
    @PutMapping("/enroll")
    public void enrollToClass(@RequestParam(value = "classes") final Set<StudentClass> enrollClasses) {
        final UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();

        studentClassService.enrollStudentToClasses(principal.getUsername(), enrollClasses, EnrollOperation.SUBSCRIBE);
    }

    /***
     * This method is used to cancel the enrollment to the selected classes for the authenticated user
     *
     * @param cancelClasses - set of selected classes from which the user should be removed
     */
    @PutMapping("/cancelEnrollment")
    public void cancelEnrollment(@RequestParam(value = "classes") final Set<StudentClass> cancelClasses) {
        final UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();

        studentClassService.enrollStudentToClasses(principal.getUsername(), cancelClasses, EnrollOperation.UNSUBSCRIBE);
    }
}
