package si.matej.paurustest.task1.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.matej.paurustest.task1.model.Student;
import si.matej.paurustest.task1.model.StudentClass;
import si.matej.paurustest.task1.repository.StudentClassRepository;
import si.matej.paurustest.task1.repository.StudentRepository;

@Service
public class StudentClassService {

    private StudentRepository studentRepository;
    private StudentClassRepository studentClassRepository;

    @Autowired
    public StudentClassService(final StudentRepository studentRepository,
                               final StudentClassRepository studentClassRepository) {

        this.studentRepository = studentRepository;
        this.studentClassRepository = studentClassRepository;
    }

    public List<StudentClass> getAllClasses() {
        return studentClassRepository.findAll();
    }

    public List<StudentClass> findByName(final String name) {
        return studentClassRepository.findByName(name);
    }

    /***
     *
     * @param username - username of the requesting user
     * @param enrollClasses - the classes that need to be updated (enroll/cancel enrollment of the user)
     * @param operation - the operation to be done on the StudentClass entity with the requesting user (enroll/cancel enrollment)
     */
    public void enrollStudentToClasses(final String username, final Set<StudentClass> enrollClasses,
                                       final EnrollOperation operation) {
        final Student student = studentRepository.findByHash(username);

        if (student == null) {
            return;
        }

        for (final StudentClass studentClass : enrollClasses) {
            final StudentClass classToUpdate = studentClassRepository.findById(studentClass.getId()).orElse(null);

            if (classToUpdate == null) {
                continue;
            }

            final List<Student> students = classToUpdate.getStudents();

            if (operation == EnrollOperation.SUBSCRIBE) {
                students.add(student);
            } else if (operation == EnrollOperation.UNSUBSCRIBE) {
                students.remove(student);
            }

            studentClassRepository.save(studentClass);
        }
    }

    /***
     * Used to define the operation that the method 'enrollStudentToClasses' should execute
     */
    public enum EnrollOperation {
        SUBSCRIBE,
        UNSUBSCRIBE
    }
}
