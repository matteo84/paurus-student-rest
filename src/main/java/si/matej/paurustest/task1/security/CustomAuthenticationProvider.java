package si.matej.paurustest.task1.security;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import si.matej.paurustest.task1.model.Student;
import si.matej.paurustest.task1.repository.StudentRepository;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private StudentRepository studentRepository;

    public CustomAuthenticationProvider() {
        super();
    }

    /***
     *
     *  This custom authentication serves to authenticate the student that is accessing the REST api.
     *  It takes the password from the basic authentication (where the student's unique hash is sent) and sets it
     *  as the User's username. Here it would be maybe more wise to use the user's ID since the student's hash is
     *  sensitive data which is used to log in. But for this PoC it doesn't matter much.
     */
    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        final String userHash = authentication.getCredentials().toString();
        if (userHash != null) {
            final Student student = studentRepository.findByHash(userHash);

            if (student != null) {
                final List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
                grantedAuthorityList.add(new SimpleGrantedAuthority("ROLE_USER"));

                final UserDetails principal = new User(student.getHash(), student.getHash(), grantedAuthorityList);

                return new UsernamePasswordAuthenticationToken(principal, userHash, grantedAuthorityList);
            }
        }
        return null;
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
