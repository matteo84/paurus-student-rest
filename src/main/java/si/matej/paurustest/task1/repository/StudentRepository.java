package si.matej.paurustest.task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import si.matej.paurustest.task1.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByHash(final String hash);
}
