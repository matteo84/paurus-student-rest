package si.matej.paurustest.task1.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import si.matej.paurustest.task1.model.StudentClass;

@Repository
public interface StudentClassRepository extends JpaRepository<StudentClass, Long> {
    List<StudentClass> findByNameContaining(final String name);

    List<StudentClass> findByName(final String name);
}
