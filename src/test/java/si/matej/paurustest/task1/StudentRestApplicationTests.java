package si.matej.paurustest.task1;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import si.matej.paurustest.task1.model.Student;
import si.matej.paurustest.task1.model.StudentClass;

@RunWith(SpringRunner.class)
@SpringBootTest
class StudentRestApplicationTests {

    private static final String JOHN_HASH = "b5cad945ecc4f5456b1755d0997ade17";
    private static final String JANE_HASH = "52d8877b20d2bd72197080e816e19d1f";
    private static final String JOE_HASH = "e8ada6cd9bb8d2d1f3150dc383308d4d";

    private static final String MACHINE_LEARNING_CLASS_NAME = "machine learning";
    private static final String MARKETING_CLASS_NAME = "marketing";
    private static final String ALGORITHMS_CLASS_NAME = "algorithms";

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    /***
     * Testing the retrieve of all classes - expected failure since the user is not authenticated
     */
    @Test
    public void testListClassesWithoutLoggedIn_expectUnauthorized() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/class/all")).andExpect(status().isUnauthorized());
    }

    /***
     * Testing the retrieve of all classes - expected success with authenticated user
     */
    @Test
    @WithMockUser(username = JOHN_HASH)
    public void testListClassesWithUser_expectOK_and_3results() throws Exception {
        final ResultActions result = mvc.perform(MockMvcRequestBuilders.get("/api/class/all"));

        result.andExpect(status().isOk());
        final String contentStringJson = result.andReturn().getResponse().getContentAsString();
        final List list = objectMapper.readValue(contentStringJson, List.class);

        Assertions.assertThat(list.size() == 3);
    }

    /***
     * Test finding of a class by it's name - failure (no authentication)
     */
    @Test
    public void testFindClassByName_unauthorized() throws Exception {
        final ResultActions result = mvc
                .perform(MockMvcRequestBuilders.get("/api/class/search/findByName")
                                 .param("name", MACHINE_LEARNING_CLASS_NAME)).andExpect(status().isUnauthorized());
    }


    /***
     * Test finding of a class by it's name - success
     */
    @Test
    @WithMockUser(username = JOHN_HASH)
    public void testFindClassByName_ok() throws Exception {
        final ResultActions result = mvc
                .perform(MockMvcRequestBuilders.get("/api/class/search/findByName")
                                 .param("name", "machine learning"));

        final String contentStringJson = result.andReturn().getResponse().getContentAsString();
        final List list = objectMapper.readValue(contentStringJson, List.class);

        Assertions.assertThat(
                list.size() == 1 && list.get(0) instanceof StudentClass && ((StudentClass) list.get(0)).getName()
                        .equals("machine learning"));
    }


    /***
     * Enrollment, Cancel enrollment test
     */
    @Test
    @WithMockUser(username = JANE_HASH)
    public void testEnrollCancelEnrollmentJaneToMarketingAndMachineLearning_ok() throws Exception {

        final StudentClass marketing = getStudentClassByName(MARKETING_CLASS_NAME);
        final StudentClass machine = getStudentClassByName(MACHINE_LEARNING_CLASS_NAME);


        //Enrolling Jane to marketing and machine learning classes
        mvc.perform(MockMvcRequestBuilders.put("/api/class/enroll")
                            .param("classes", "" + machine.getId(), "" + marketing.getId())).andExpect(status().isOk());


        //expecting Jane to be present on the marketing class
        final List<Student> students = getStudentClassByName(MARKETING_CLASS_NAME).getStudents();
        Assertions.assertThat(students.size() == 1);
        Assertions.assertThat(students.get(0).getHash().equals(JANE_HASH));

        //at this stage we have one person (Jane) enrolled to the machine learning class
        Assertions.assertThat(getStudentClassByName(MACHINE_LEARNING_CLASS_NAME).getStudents().size() == 1);

        //cancel Jane's enrollment to machine learning class
        mvc.perform(MockMvcRequestBuilders.put("/api/class/cancelEnrollment")
                            .param("classes", "" + machine.getId())).andExpect(status().isOk());

        //the class should now be empty
        Assertions.assertThat(getStudentClassByName(MACHINE_LEARNING_CLASS_NAME).getStudents().isEmpty());
    }

    private StudentClass getStudentClassByName(final String name) throws Exception {
        final String resultJson = mvc
                .perform(MockMvcRequestBuilders.get("/api/class/search/findByName")
                                 .param("name", name)).andReturn().getResponse().getContentAsString();

        final Class<?> clz = Class.forName(StudentClass.class.getName());
        final JavaType type = objectMapper.getTypeFactory().constructCollectionType(List.class, clz);
        final List<StudentClass> result = objectMapper.readValue(resultJson, type);

        return result.get(0);
    }
}
