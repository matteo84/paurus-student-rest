# Simple student IS
This is a simple student information system REST application.

It supports the following operations:
* list all available classes 
* basic search of classes by name
* enroll to one or many classes
* cancel enrollment to a class

# Starting the application

* In IntelliJ IDEA you can run/debug the StudentRestApplication or StudentRestApplicationTests

* In a terminal using maven you can run the application by running the command: 
    mvn spring-boot:run
    
# Basic info

- The application uses an in-memory H2 database which is wiped on each run.
- resources/data.sql contains the initial data which is inserted to the DB at startup
- all the /api/* endpoints are secured and required the user to be authenticated
- the authentication is of type Basic Auth and uses the Student's hash as password (no username)
